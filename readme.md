<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>


## About Repository

Laravel Email Verification with forget password email.

- Signup email verification
- Forget password email with activation link



## Clone Repository
git clone https://gitlab.com/rahulsingh92/laravel-email-verification.git

## Create database and migrate
php artisan migrate


## Email Settings in .env file

MAIL_DRIVER=smtp

MAIL_HOST=smtp.gmail.com

MAIL_PORT=587

MAIL_USERNAME=your-email

MAIL_PASSWORD=your-password

MAIL_ENCRYPTION=tls

that's it