<!DOCTYPE html>
<html>
<head>
	<title>Email Template</title>
	<style type="text/css">
        body{font-family: 'Open Sans','Helvetica','Arial',sans-serif!important;}
		.right-side{margin-left: 25%;background:#f2f2f2;background-size:cover;min-height:400px;width:50%;}
		.logo-center{text-align: center;}
		.button { background-color: #4CAF50;color: white;padding: 15px 32px;text-decoration: none;border-radius: 12px;}
		.img{margin-left: 6%;}
    </style>
</head>
<body>
<div class="right-side">
    <div class="logo-center"><br>
    	<img src="http://www.techiva.com/techiva_latest/images/logo.png" class="img"><br><br>
    	<div>
    		<h2> Welcome  {{ $first_name }} {{ $last_name }},</h2>
    		<h3> Please confirm your email</h3><br>
    		<a class="button" href="{{ url('user/activation', $link)}}">CONFIRM EMAIL</a><br><br>
    		<p>© 2017 Techiva. All rights reserved </p>
    	</div>
    </div>
</div>
</body>
</html>