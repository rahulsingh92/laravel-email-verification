@extends('layouts.app')
@section('content')
<div class="container">
    <div class="col-md-10 col-md-offset-1 " >
        <div class="col-md-8 col-md-offset-3 right-side" >
            <h3>Register</h3>
            <!--Form with header-->
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <div class="form">
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" name="first_name" value="{{ old('first_name') }}" placeholder="First name">
                        @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
					<div class="form-group">
                        <input type="text" class="form-control input-lg" name="last_name" value="{{ old('last_name') }}" placeholder="Last name">
                        @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control input-lg" name="email" value="{{ old('email') }}" placeholder="Enter your email">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" name="password" placeholder="password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
					  <div class="form-group">
                        <input type="password" class="form-control input-lg" name="confirm_password" placeholder="Confirm password">
                        @if ($errors->has('confirm_password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('confirm_password') }}</strong>
                            </span>
                        @endif
                    </div>
					
					<div class="form-group">
                        <input type="number" class="form-control input-lg" name="phone" value="{{ old('phone') }}" placeholder="Phone number">
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
					
					<div class="form-group">
					   <select class="form-control input-lg" id="sel1" name="user_type">
							<option>User Type</option>
							<option class="" value="Landlord">Landlord</option>
		                    <option class="" value="Tenant">Tenant</option>
							
						  </select>
                       
                        @if ($errors->has('user_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('user_type') }}</strong>
                            </span>
                        @endif
                    </div>
                   
				   
					<div class="form-group">
                        <input type="text" class="form-control input-lg" name="company_name" value="{{ old('company_name') }}" placeholder="Company name">
                        @if ($errors->has('company_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('company_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-deep-purple">Register</button>
                   
                </div>
            </form>
            <!--/Form with header-->
        </div><!--col-sm-6-->
    </div><!--col-sm-8-->
</div><!--container-->
@endsection

