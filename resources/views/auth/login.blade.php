@extends('layouts.app')
@section('content')
<div class="container">
    <div class="col-md-10 col-md-offset-1 " >
        <div class="col-md-8 col-md-offset-3 right-side" >
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('warning'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <h3>Login</h3>
            <!--Form with header-->
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form">
                    <div class="form-group">
                        <input type="email" class="form-control input-lg" name="email" value="{{ old('email') }}" placeholder="Enter your email">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" name="password" placeholder="Enter your password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-deep-purple">Login</button>
                    <a class="btn btn-link" href="{{ route('password.request') }}">Forgot Your Password?</a>
                </div>
            </form>
            <!--/Form with header-->
        </div><!--col-sm-6-->
    </div><!--col-sm-8-->
</div><!--container-->
@endsection
       